﻿using System;
using System.Collections.Generic;

namespace DataLayer.Models
{
    public class Course
    {
        // public string Course;
        public int Id { get; set; }

        public string CourseName { get; set; }
        public string TrainerName { get; set; }
        public DateTime CourseStartDate { get; set; }
        public int HomeworkThreshold { get; set; }
        public int PresenceThreshold { get; set; }

        public virtual ICollection<Student> Students { get; set; }
        public virtual ICollection<Homework> Homeworks { get; set; }
        public virtual ICollection<ClassDay> ClassDays { get; set; }

        public int AmountOfStudents { get; set; }
        public int AmountOfCourseDays { get; set; }

        //public virtual ICollection<Student> StudentsTable { get; set; }
        public Course()
        {
            Students = new List<Student>();
            Homeworks = new List<Homework>();
            ClassDays = new List<ClassDay>();
        }
    }
}