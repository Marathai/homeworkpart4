﻿using System;
using System.Collections.Generic;

namespace DataLayer.Models
{
    public class ClassDay
    {
        public int Id { get; set; }
        public DateTime ClassDate { get; set; }
        public virtual ICollection<Presence> Presense { get; set; }

        public ClassDay()
        {
            Presense = new List<Presence>();
        }
    }
}