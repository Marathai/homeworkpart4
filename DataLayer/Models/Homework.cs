﻿using System.Collections.Generic;

namespace DataLayer.Models
{
    public class Homework
    {
        public int Id { get; set; }
        public int RequiredPoints { get; set; }
        public virtual ICollection<EarnedPoints> EarnedPoints { get; set; }

        public Homework()
        {
            EarnedPoints = new List<EarnedPoints>();
        }
    }
}