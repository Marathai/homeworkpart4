﻿namespace DataLayer.Models
{
    public class EarnedPoints
    {
        private object earnedPoints1;
        private Student student1;

        public int Id { get; set; }
        public Homework homework { get; set; }
        public Student student { get; set; }
        public int earnedPoints { get; set; }

        public EarnedPoints(Homework homework, Student student, int earnedPoints)
        {
            this.homework = homework;
            this.student = student;
            this.earnedPoints = earnedPoints;
        }
    }
}