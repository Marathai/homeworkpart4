﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Presence
    {
        public int Id { get; set; }
        public ClassDay classDay { get; set; }
        public Student student { get; set; }
        public bool isPresent { get; set; }

        public Presence(ClassDay classDay, Student student, bool isPresent)
        {
            this.classDay = classDay;
            this.student = student;
            this.isPresent = isPresent;
        }

        public Presence(int id, ClassDay classDay, Student student, bool isPresent)
        {
            Id = id;
            this.classDay = classDay;
            this.student = student;
            this.isPresent = isPresent;
        }
    };
    
}
