﻿using System;
using System.Collections.Generic;
using System.Linq;
using Homework1.Services;

namespace CourseLogicLayer.Dtos
{
    public class CourseDto
    {
        public int Id;
        public string CourseName;
        public string TrainerName;
        public DateTime CourseStartDate;
        public int HomeworkThreshold;
        public int PresenceThreshold;

        public List<StudentDto> Students;
        public List<HomeworkDto> Homeworks;
        public List<ClassDayDto> ClassDays;

        public int AmountOfStudents;
        public int AmountOfCourseDays;

        public CourseDto()
        {
            Students = new List<StudentDto>();
            Homeworks = new List<HomeworkDto>();
            ClassDays = new List<ClassDayDto>();
        }

        public int StudentPresence(StudentDto student)
        {
            return ClassDays.Count(day => day.Presense.ContainsKey(student) && day.Presense[student].isPresent);
        }

        public int StudentPoints(StudentDto student)
        {
            int points = 0;
            foreach (var homeworkDto in Homeworks)
            {
                points += homeworkDto.getStudentPoints(student);
            }
            return points;
        }

        public void AddHomework(HomeworkDto homework)
        {
            var service = new CourseService();
            Homeworks.Add(homework);
            service.AddHomeworkToCourse(homework, this);
        }

        public void AddStudentToCourse(StudentDto student)
        {
            try
            {
                var service = new CourseService();
                Students.Add(student);
                student.Courses.Add(this);
                service.AddStudentToCourse(student, this);
            }
            catch (NullReferenceException e)
            {
                Console.WriteLine("Operacja zakonczona niepowodzeniem");
            }
        }

        public void RemoveStudentFromCourse(StudentDto student)
        {
            try
            {
                var service = new CourseService();
                Students.Remove(student);
                student.Courses.Remove(this);
                service.RemoveStudentFromCourse(student, this);
            }
            catch (NullReferenceException)
            {
                Console.WriteLine("Operacja zakonczona niepowodzeniem");
            }
        }

        public bool AddClassDay(ClassDayDto classDay)
        {
            var service = new CourseService();
            ClassDays.Add(classDay);
            return service.AddClassDayToCourse(classDay, this);
        }
    }
}