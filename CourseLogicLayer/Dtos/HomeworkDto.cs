﻿using System.Collections.Generic;

namespace CourseLogicLayer.Dtos
{
    public class HomeworkDto
    {
        public int Id;
        public int RequiredPoints { get; set; }
        public Dictionary<StudentDto, int> EarnedPoints = new Dictionary<StudentDto, int>();

        public HomeworkDto(int requiredPoints)
        {
            RequiredPoints = requiredPoints;
        }

        public HomeworkDto()
        {
        }

        public void addStudentPoints(StudentDto studentDto, int points)
        {
            EarnedPoints.Add(studentDto, points);
        }

        public int getStudentPoints(StudentDto studentDto)
        {
            if (EarnedPoints.ContainsKey(studentDto))
            {
                return EarnedPoints[studentDto];
            }
            else
            {
                return 0;
            }
        }
    }
}