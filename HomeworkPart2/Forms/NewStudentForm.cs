﻿using System;
using CourseLogicLayer.Dtos;
using Homework1;

namespace HomeworkPart2.Forms
{
    public class NewStudentForm
    {
        public static StudentDto ShowForm()
        {
            Console.WriteLine("Podaj dane studenta");

            StudentDto kursant = new StudentDto();

            Console.WriteLine();

            kursant.Pesel = ConsoleInput.GetPesel("Podaj pesel");

            Console.WriteLine(" imie");
            kursant.Name = Console.ReadLine();

            Console.WriteLine(" nazwisko");
            kursant.Surname = Console.ReadLine();

            kursant.Birthday = ConsoleInput.GetDateTime(" data urodzenia [dd/mm/yyyy]");

            kursant.StudentGender = (StudentDto.Gender) ConsoleInput.GetEnum(typeof(StudentDto.Gender),
                " podaj plec kursanta (do wyboru: Female/Male/Other");

            return kursant;
        }
    }
}